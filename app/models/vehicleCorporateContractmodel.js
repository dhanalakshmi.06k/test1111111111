
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let vehicleCorporateContractSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }

},{strict:false},{collection: "vehicleCorporateContract"});

let vehicleCorporateContract = mongoose.model('vehicleCorporateContract',vehicleCorporateContractSchema);
vehicleCorporateContractSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
vehicleCorporateContractSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
vehicleCorporateContractSchema.pre('findOneAndUpdate', function (next) {
    this.updated = new Date();
    next();
});
module.exports = vehicleCorporateContract;

