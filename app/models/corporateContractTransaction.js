/**
 * Created by Suhas on 7/5/2016.
 */

let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let companyTransactionSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "companyTranasactionDetails"});
let companyContractTranasaction = mongoose.model('companyTranasactionDetails',companyTransactionSchema);
companyTransactionSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = companyContractTranasaction;



/*
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let cctypeSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }

},{strict:false},{collection: "corporateContractTransaction"});

let corporateContractTransaction= mongoose.model('corporateContractTransaction',cctypeSchema);
cctypeSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
cctypeSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
cctypeSchema.pre('findOneAndUpdate', function (next) {
    this.updated = new Date();
    next();
});
module.exports = corporateContractTransaction;
*/
