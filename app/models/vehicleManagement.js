var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ElectionResultSchema = new mongoose.Schema(
{
       ownerDetails:String,
       vehicleRegisterNumber:Number,
       vehicleInsurance:String,
       typeOfVehicle:String,
       driverName:String,
       licenseNumber:Number,
       expiryDate:Date,
       address:String,
       joiningDate:Date,
       otherDetails:String
},
{collection:"vehicleManagementDetails"});
mongoose.model('vehicleManagementDetails',ElectionResultSchema);