var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    LessorModel = mongoose.model('lessorType');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/lessorDetails', function(req, res, next) {
    var lessorModel = new LessorModel(req.body);
    lessorModel.save(function(err, result) {
        if (err){
            console.log('lessorDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});


router.get('/allLessorDetails/:start/:range', function(req, res, next) {
    LessorModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/lessorBymongoId/:lessorMongoId',function(req,res,next){

        LessorModel.findOne({"_id":req.params.lessorMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});

router.get('/lessorType/count', function(req, res, next) {
    LessorModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})

router.post('/editLessorBymongoId/:lesserId', function(req, res, next) {

        LessorModel.findOneAndUpdate({"_id":req.params.lesserId},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/lessorBymongoId/:lessorMongoId',function(req, res, next){

            LessorModel.remove({"_id":req.params.lessorMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




