var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    VehicleModel = mongoose.model('vehicleDetails');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/vehicleDetails', function(req, res, next) {

    var vehicleModel = new VehicleModel(req.body);
    vehicleModel.save(function(err, result) {
        if (err){
            console.log('vehicleDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/allVehicleTypeForDropDown', function(req, res, next) {
    VehicleModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.get('/allVehicleTypeDetails/:start/:limit', function(req, res, next) {
    VehicleModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.limit))

})


router.get('/vehicleBymongoId/:vehicleMongoId',function(req,res,next){

        VehicleModel.findOne({"_id":req.params.vehicleMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editVehicleBymongoId/:vehicleMongoId', function(req, res, next) {

        VehicleModel.findOneAndUpdate({"_id":req.params.vehicleMongoId},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})



router.get('/VehicleDetails/count', function(req, res, next) {
    VehicleModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})



router.delete('/deleteVehicleBymongoId/:vehicleMongoId',function(req, res, next){

            VehicleModel.remove({"_id":req.params.vehicleMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




