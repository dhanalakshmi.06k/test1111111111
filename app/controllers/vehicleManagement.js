var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    VehicleManagementModel = mongoose.model('vehicleManagementDetails');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/vehicleManagementDetails', function(req, res, next) {

    var vehicleManagementModel = new VehicleManagementModel(req.body);
    vehicleManagementModel.save(function(err, result) {
        if (err){
            console.log('vehicleDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});


router.get('/allVehicleManagementDetails', function(req, res, next) {
    VehicleManagementModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/vehicleManagementBymongoId/:vehicleManagementMongoId',function(req,res,next){

        VehicleManagementModel.findOne({"_id":req.params.vehicleManagementMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editVehicleManagementBymongoId', function(req, res, next) {

        VehicleManagementModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/vehicleBymongoId/:vehicleManagementMongoId',function(req, res, next){

            VehicleManagementModel.remove({"_id":req.params.vehicleManagementMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




