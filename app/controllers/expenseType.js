var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ExpenseModel = mongoose.model('expenseType');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/expenseDetails', function(req, res, next) {

    var expenseModel = new ExpenseModel(req.body);
    expenseModel.save(function(err, result) {
        if (err){
            console.log('expenseTypeDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});


router.get('/allExpenseDetails/count', function(req, res, next) {
    ExpenseModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})


router.get('/allExpenseDetails/:start/:range', function(req, res, next) {
    ExpenseModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/expenseBymongoId/:expenseMongoId',function(req,res,next){
        ExpenseModel.findOne({"_id":req.params.expenseMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editExpenseBymongoId/:expenseTypeId', function(req, res, next) {

        ExpenseModel.findOneAndUpdate({"_id":req.params.expenseTypeId},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/expenseBymongoId/:expenseMongoId',function(req, res, next){

            ExpenseModel.remove({"_id":req.params.expenseMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




