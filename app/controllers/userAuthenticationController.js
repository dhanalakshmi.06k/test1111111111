var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose')
let user = require('../models/userDetails');
let userDataAccess = require("../dao").user;
let fs = require("fs");
let jwt= require('jsonwebtoken');
let bCrypt = require('bcrypt-nodejs');
let path = require('path')
let config=require('../../config/config');

module.exports = function (app){
    app.use('/', router);
};

router.post('/signUp', function(req, res, next) {
    console.log(req.body)
    var pwd=req.body.password;
    // find a user in Mongo with provided username
    user.findOne({'username':req.body.username},function(err, user) {
        // In case of any error return
        if (err){
            res.status(401).send('User does not exists register to the application');
        }
        // already exists
        if (user) {
            res.status(401).send('User already exists');

        } else {
            // if there is no user with that email
            // create the user
            userDataAccess.saveUserDetails(req.body)
                .then(function(result){
                    res.send(result);
                })
        }
    });
});

router.post('/loginIn', function(req, res, next) {
    console.log(req.body)
    var pwd=req.body.password;
    user.findOne({ 'username' :  req.body.username },
        function(err, user) {
            // In case of any error, return using the done method
            if (err)
                return done(err);
            // Username does not exist, log error & redirect back
            if (!user){
                res.send('Wrong user or password');
                return;
            }
            // User exists but wrong password, log the error
            var status=isValidPassword(user, pwd );
            if (!isValidPassword(user, pwd )){
                res.send('Wrong  password');
                return;
            }
            var token=generateToken(user)

            res.json({ token: token });
        }
    );
});







function generateToken(user){
    // User and password both match, return user from
    // done method which will be treated like success
    // We are sending the profile inside the token
    let filePath=path.join(__dirname, '/../../public.key');
    var cert = fs.readFileSync(filePath);
    var obj={}
    obj.userDetails=restructureObj(user)
    var genToken = jwt.sign(obj, cert, { algorithm: 'RS256'}, {
        expiresIn: config.PARAMETERS.SESSION_EXPIRE_TIME// expires in 60seconds
    });

    return genToken;
}
var isValidPassword = function(user, pwd){
    return bCrypt.compareSync(pwd, user.password);
}


function restructureObj(obj){
    var modifiedObj={}
    modifiedObj.username=obj.username
    modifiedObj.firstName=obj.firstName
    modifiedObj.lastName=obj.lastName
    modifiedObj.email=obj.email
    return modifiedObj

}



