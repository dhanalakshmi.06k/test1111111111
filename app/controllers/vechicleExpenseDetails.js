var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    vechicleExpenseModel = mongoose.model('vechicleExpenseDetails');

module.exports = function (app){
    app.use('/', router);
};


router.post('/saveVechicleExpense', function(req, res, next) {

    var vechicleExpense = new vechicleExpenseModel(req.body);
    vechicleExpense.save(function(err, result) {
        if (err){
            console.log('vechicleExpense failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/allVechicleExpense/count', function(req, res, next) {
    vechicleExpenseModel.count(function(err,expenseCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: expenseCount};
        res.send(count);
    });
})

router.get('/allVechicleExpenseBasedOnExpenseType/:expenseType', function(req, res, next) {
    vechicleExpenseModel.find({"expenseType":req.params.expenseType},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})



router.get('/allVechicleExpense/:start/:range', function(req, res, next) {
    vechicleExpenseModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/vechicleExpenseBymongoId/:vechicleExpenseMongoId',function(req,res,next){
    vechicleExpenseModel.findOne({"_id":req.params.vechicleExpenseMongoId},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});



router.post('/editVechicleExpense/:vechicleExpenseId', function(req, res, next) {
    vechicleExpenseModel.findOneAndUpdate({"_id":req.params.vechicleExpenseId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})


router.delete('/deleteVechicleExpense/:vechicleExpenseId',function(req, res, next){

    vechicleExpenseModel.remove({"_id":req.params.vechicleExpenseId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});




