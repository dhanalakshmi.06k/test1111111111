var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose');
    var assetDataAccess = require("../dao").assets;
    var assetConfigDao = require('../dao/assetConfig');
    var _ = require("lodash");
let assetConfigDAO = require("../dao").assetConfig;
let assetModel = require('../models/assets')
module.exports = function (app){
    app.use('/', router);
};

router.delete('/deleteAssetById/:asseTMongodbId',function(req, res, next){
   
    assetModel.remove({"_id":req.params.asseTMongodbId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});



//http://localhost:5100/count/assetName?type=car
router.get('/count/assetName', function(req, res, next) {

    let type = req.query.type;
    if(type){
        type=_.split(type,",");
    }
    assetDataAccess.getCount(type)
        .then(function(assetCount){
            res.send({count:assetCount});
        })
        .catch(function(err){
            res.status(400).send({msg:err});
        })
});


//:return all Assets types

//http://localhost:5100/types
router.get('/types', function(req, res, next) {
    assetConfigDAO.getAllTypes()
        .then(function(allAssets){
            res.send(allAssets);
        })
});



//get all the asset configuration



router.get('/assetConfig', function(req, res, next) {
    let methodToBeExec =null;
    if(req.query.type){
        methodToBeExec = assetConfigDAO.getByType(req.query.type);
    }else{
        methodToBeExec = assetConfigDAO.getAll();
    }
    methodToBeExec.then(function(allAssets){
        res.send(allAssets);
    })
});